Introduction-to-R
=================

I started this book because I felt that the books I could find was not speaking
a language accessible to people that did not already have a good grasp of at
least computers in general and some familiarity with scripts.
I therefore started writing a introduction in a language that I hoped would be
more friendly.
The project is however currently **abandoned**.
Some of the information is also obsolete. For example:

* There are now better packages than those used in chapter 6 for importing data.
`readr`, `readxl`, `writexl` are better alternatives.
* The melt/cast concepts in chapter 9 are now better handles by the `dplyr` and 
`tidyr` packages.
* Were I to write it again I would focus on tidyverse concepts.

